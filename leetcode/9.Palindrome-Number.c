#include <stdbool.h>
bool isPalindrome(unsigned x)
{
// uf x is less than 0, not palindrome
    if (x<0)
    {
        return false;
    }
//declare all variables
    unsigned digit, sum, num, i = 0;;
    num=x;
//loop through given num. adding to a num
    while (num>0)
    {
    	digit=num%10;
        sum=(10*sum)+digit;
        num=num/10;
    }
    //if sum of digits equals to given argument, num is palindrome. Else num is not palindrome, include num that is 12340 for example
    if (sum == x)
    {
        return true;
    }
    else
    {
        return false;
    }
}

